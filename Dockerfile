FROM openjdk:11-jdk
COPY . /


FROM openjdk:11-jdk
COPY --from=0  target/*.jar app.jar
ENV SERVER_PORT=8000
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
EXPOSE 8000